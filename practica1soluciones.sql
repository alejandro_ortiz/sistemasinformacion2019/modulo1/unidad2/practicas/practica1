﻿USE practica1;

-- 1. Mostrar todos los campos y todos los registros de la tabla empleado

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e;

-- 2. Mostrar todos los campos y todos los registros de la tabla departamento

  SELECT 
    d.dept_no,
    d.dnombre,
    d.loc 
  FROM 
    depart d;

-- 3. Mostrar el apellido y oficio de cada empleado

  SELECT 
    e.apellido, 
    e.oficio 
  FROM 
    emple e;

-- 4. Mostrar localización y número de cada departamento

  SELECT 
    d.loc, 
    d.dept_no 
  FROM 
    depart d;

-- 5. Mostrar el número, nombre y localización de cada departamento

  SELECT 
    d.dept_no, 
    d.dnombre, 
    d.loc 
  FROM 
    depart d;

-- 6.  Indicar el número de empleados que hay 

  SELECT 
    COUNT(*) 
  FROM 
    emple e;

-- 7. Datos de los empleados ordenados por apellido de forma ascendente 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  ORDER BY 
    e.apellido ASC;

-- 8. Datos de los empleados ordenados por apellido de forma descendente 

   SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  ORDER BY 
    e.apellido DESC;

-- 9. Indicar el numero de departamentos que hay 

  SELECT 
    COUNT(*) 
  FROM 
    depart d;

-- 10. Indicar el número de empleados mas el numero de departamentos 

  SELECT
    (SELECT 
      COUNT(*) 
    FROM 
      emple e)
    +
    (SELECT 
      COUNT(*) 
    FROM 
      depart d) 
  AS suma ;

-- 11. Datos de los empleados ordenados por número de departamento descendentemente. 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  ORDER BY 
    e.dept_no DESC;

-- 12. Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  ORDER BY 
    e.dept_no DESC,
    e.oficio ASC;

-- 13. Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente. 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  ORDER BY 
    e.dept_no DESC,
    e.apellido ASC;

-- 14. Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.

  SELECT 
    DISTINCT e.emp_no 
  FROM 
    emple e 
  WHERE 
    e.salario>2000;

-- 15. Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000. 

  SELECT 
    DISTINCT e.emp_no,e.apellido 
  FROM 
    emple e 
  WHERE 
    e.salario<2000;

-- 16. Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  WHERE 
    e.salario BETWEEN 1500 AND 2500;

-- 17. Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ. 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  WHERE 
    e.oficio='Analista';

-- 18. Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 € 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no   
  FROM 
    emple e 
  WHERE 
    e.oficio='Analista' 
    AND 
    e.salario>2000;

-- 19. Seleccionar el apellido y oficio de los empleados del departamento número 20. 

  SELECT 
    e.apellido,
    e.oficio 
  FROM 
    emple e 
  WHERE 
    e.dept_no=20;

-- 20. Contar el número de empleados cuyo oficio sea VENDEDOR 

  SELECT 
    COUNT(*) 
  FROM 
    emple e 
  WHERE 
    e.oficio='Vendedor';

-- 21. Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente. 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  WHERE 
    e.apellido LIKE 'm%'
  UNION
  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no
  FROM 
    emple e 
  WHERE 
    e.apellido LIKE 'n%';

-- 22. Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos ordenados por apellido de forma ascendente.
  
  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  WHERE 
    e.oficio='Vendedor' 
  ORDER BY 
    e.apellido ASC;

-- 23. Mostrar los apellidos del empleado que mas gana 

  SELECT 
    e.apellido 
  FROM 
    emple e 
  WHERE 
    e.salario=(SELECT MAX(e1.salario) FROM emple e1);

-- 24. Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. Ordenar el resultado por apellido y oficio de forma ascendente 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  WHERE 
    e.dept_no=10 
    AND 
    e.oficio='Analista' 
  ORDER BY 
    e.apellido ASC, 
    e.oficio ASC;

-- 25. Realizar un listado de los distintos meses en que los empleados se han dado de alta 

  SELECT 
    DISTINCT MONTHNAME(e.fecha_alt) 
  FROM 
    emple e;

-- 26. Realizar un listado de los distintos años  en que los empleados se han dado de alta 

  SELECT 
    DISTINCT YEAR(e.fecha_alt) 
  FROM 
    emple e;

-- 27. Realizar un listado de los distintos días del mes en que los empleados se han dado de alta 

  SELECT 
    DISTINCT DAYOFMONTH(e.fecha_alt) 
  FROM 
    emple e;

  SELECT 
    DISTINCT DAY(e.fecha_alt) 
  FROM 
    emple e;

-- 28. Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20. 

-- Solucion con OR

  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  WHERE 
    e.salario>2000 
    OR 
    e.dept_no=20;

-- Solucion con UNION

  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  WHERE 
    e.salario>2000
    UNION
  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  WHERE 
    e.dept_no=20;

-- 29. Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece 

  SELECT 
    e.apellido, 
    d.dnombre 
  FROM 
    emple e 
  JOIN 
    depart d 
  USING 
    (dept_no);

-- 30. Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. Ordenar los resultados por apellido de forma descendente 

  SELECT 
    e.apellido, 
    e.oficio,
    d.dnombre 
  FROM 
    emple e 
  JOIN 
    depart d 
  USING 
    (dept_no)
  ORDER BY
    e.apellido DESC;

-- 31. Listar el número de empleados por departamento.La salida del comando debe ser como la que vemos a continuación 
  
  SELECT 
    e.dept_no, 
    COUNT(*)NUMERO_DE_EMPLEADOS 
  FROM 
    emple e 
  GROUP BY 
    e.dept_no;

-- 32. Realizar el mismo comando anterior pero obteniendo una salida como la que vemos
  
  SELECT 
    d.dnombre, 
    COUNT(*)NUMERO_DE_EMPLEADOS 
  FROM 
    emple e 
  JOIN 
    depart d 
  USING 
    (dept_no)
  GROUP BY 
    d.dnombre; 
  
  -- otra forma más óptima 
  SELECT 
    d.dnombre, 
    c1.NUMERO_DE_EMPLEADOS 
  FROM (
    SELECT 
      e.dept_no, 
      COUNT(*)NUMERO_DE_EMPLEADOS 
    FROM 
      emple e 
    GROUP BY 
      e.dept_no) c1
  JOIN 
    depart d 
  USING 
    (dept_no);

-- 33. Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre. 

  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  ORDER BY 
    e.oficio ASC, 
    e.apellido ASC; 

-- 34. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ. Listar el apellido de los empleados 

  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  WHERE 
    e.apellido LIKE 'A%';

-- 35. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. Listar el apellido de los empleados 

  -- Con oprerador OR

  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  WHERE 
    e.apellido LIKE '%A'
    OR
    e.apellido LIKE '%M';

  -- Con operador UNION

  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  WHERE 
    e.apellido LIKE 'A%'
    UNION
  SELECT 
    DISTINCT e.apellido 
  FROM 
    emple e 
  WHERE 
    e.apellido LIKE 'M%';

-- 36. Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. Listar todos los campos de la tabla empleados 

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e 
  WHERE 
    e.apellido NOT LIKE '%Z';

-- 37. Seleccionar de la tabla EMPLE aquellas filas cuyo APELLIDO empiece por ʻAʼ y el OFICIO tenga una ʻEʼ en cualquier posición. Ordenar la salida por oficio y por salario de forma descendente

  SELECT 
    e.emp_no,
    e.apellido,
    e.oficio,
    e.dir,
    e.fecha_alt,
    e.salario,
    e.comision,
    e.dept_no 
  FROM 
    emple e
  WHERE 
    e.apellido LIKE 'A%' 
    AND 
    e.oficio LIKE '%E%'
  ORDER BY 
    e.oficio DESC,
    e.salario DESC;